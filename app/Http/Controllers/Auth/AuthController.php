<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\RegisterRequest;
use Auth;
use Session;
use Redirect;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    protected $auth;

    /**
     * Create a new authentication controller instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        //if user is not authenticated, the user will be a quest
        $this->auth = $auth;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show the application login form.
     *
     * @return Response
     */
    public function getLogin()
    {
        //get user from session if it exists
        if(Session::get('user_id')){
            return redirect('/');
        }
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  LoginRequest  $request
     * @return Response
     */
    public function postLogin(LoginRequest $request)
    {
        /**
         * Check if user exists in the database
         */
        if (Auth::attempt($request->only('gebruikersnaam', 'password')))
        {

            $user_data = Auth::user();
            /**
             * Save the userdata in a session
             */
            Session::put('user_id', $user_data->mederwerkercode);
            //Redirect to home page

            return redirect('/');
        }
        /**
         * If the user doens't exists, return to login page with an error message
         */
        return redirect('/medewerkers')->withErrors([
            'gebruikersnaam' => 'These credentials do not match our records.',
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    public function getLogout()
    {
        //Destroy user session
        Auth::logout(); // logout user
        Session::flush();
        Redirect::back();
        
        //redirect to homepage
        return redirect(\URL::previous());

    }
}
