<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;

class BestellingController extends Controller
{
    /**
     * Display a listing of all shops a user works with their orders to complete.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check if user is logged in
        if (Session::get('user_id')) {
            //Get the order, shop, colleage, customer and product values from the database
            $bestelling = DB::table('bestelling')
                ->join('winkel', 'bestelling.winkelcode', '=', 'winkel.winkelcode')
                ->join('klant', 'bestelling.klantcode', '=', 'klant.klantcode')
                ->join('medewerker', 'bestelling.mederwerkercode', '=', 'medewerker.mederwerkercode')
                ->join('artikel', 'bestelling.artikelcode', '=', 'artikel.artikelcode')
                ->select('klant.klantcode', 'klant.voorletters', 'klant.achternaam', 'klant.tussenvoegsels','winkel.winkelnaam',
                        'winkel.vestigingsplaats','medewerker.voorletters as mvoorletters', 'medewerker.achternaam as machternaam',
                        'medewerker.voorvoegsels as mvoorvoegsels', 'bestelling.aantal', 'bestelling.afgehaald', 'artikel.artikel')
                ->where('medewerker.mederwerkercode', '=', Session::get('user_id'))
                ->get();

            //Get the shop values from the database
            $winkels = DB::table('winkel')
                ->select('winkelnaam', 'vestigingsplaats', 'winkelcode')
                ->get();

            //return a view with our paramaters of our data
            return view('rapporten.bestellingen', ['bestelling' => $bestelling, 'winkels' => $winkels, 'selected' => NULL]);
        } else {
            //Creates a forbidden page
           abort(403, 'Unauthorized action.');
        }

    }

    /**
     * Display a listing of only one shop a user works with their orders to complete
     */
    public function show()
    {
        $selected_winkel = $_POST['winkel'];
        if (Session::get('user_id')) {
            $bestelling = DB::table('bestelling')
                ->join('winkel', 'bestelling.winkelcode', '=', 'winkel.winkelcode')
                ->join('klant', 'bestelling.klantcode', '=', 'klant.klantcode')
                ->join('medewerker', 'bestelling.mederwerkercode', '=', 'medewerker.mederwerkercode')
                ->join('artikel', 'bestelling.artikelcode', '=', 'artikel.artikelcode')
                ->select('klant.klantcode', 'klant.voorletters', 'klant.achternaam', 'klant.tussenvoegsels','winkel.winkelnaam', 'winkel.vestigingsplaats',
                    'medewerker.voorletters as mvoorletters', 'medewerker.achternaam as machternaam', 'medewerker.voorvoegsels as mvoorvoegsels', 'bestelling.aantal', 'bestelling.afgehaald', 'artikel.artikel')
                ->where('winkel.winkelcode', '=',$selected_winkel)
                ->where('medewerker.mederwerkercode', '=', Session::get('user_id'))
                ->get();

            $winkels = DB::table('winkel')
                ->select('winkelnaam', 'vestigingsplaats', 'winkelcode')
                ->get();
            //print_r($bestelling);die();

            return view('rapporten.bestellingen', ['bestelling' => $bestelling, 'winkels' => $winkels, 'selected' => $selected_winkel]);
        } else {
            abort(403, 'Unauthorized action.');
        }

    }


}
