@extends('layout.master')

@section('main_content')
    <div class="content">
        <h1>Winkelbestellingen voor {{Session::get('user_id')}}</h1>
        <form method="post" action="/bestellingen">
            {!! csrf_field() !!}
            <label>Zoek een winkel</label>
            <select name="winkel">
                @foreach($winkels as $w)
                    @if($selected == $w->winkelcode)
                        <option selected value="{{$w->winkelcode}}">{{$w->winkelnaam}} {{$w->vestigingsplaats}}</option>
                    @else
                        <option value="{{$w->winkelcode}}">{{$w->winkelnaam}} {{$w->vestigingsplaats}}</option>
                    @endif
                @endforeach
            </select>
            <input type="submit" value="Zoek">
        </form>
        @if($bestelling)
        <table>
            <tr>
                <th></th>
                <th>Klant</th>
                <th>Artikel</th>
                <th>Aantal</th>
                <th>Medewerker</th>
                <th>Winkel</th>
                <th>Afgehaald</th>
            </tr>
            <?php $nummer = 1;?>

            @foreach($bestelling as $b)
                <tr>
                    <td>{{$nummer++}}</td>
                    <td>{{$b->voorletters}}. {{$b->tussenvoegsels}} {{$b->achternaam}}</td>
                    <td>{{$b->artikel}}</td>
                    <td>{{$b->aantal}}</td>
                    <td>{{$b->mvoorletters}}. {{$b->mvoorvoegsels}} {{$b->machternaam}} </td>
                    <td>{{$b->winkelnaam}} {{$b->vestigingsplaats}}</td>
                    @if($b->afgehaald == True)
                        <td>Ja</td>
                    @else
                        <td>Nee</td>
                    @endif
                </tr>

            @endforeach
        </table>

        @else
            <h3>Er zijn geen bestellingen voor dit filiaal</h3>
        @endif

        <form method="post" action="/bestellingen">
            {!! csrf_field() !!}
            <label>Zoek een winkel</label>
            <select name="winkel">
                @foreach($winkels as $w)
                    @if($selected == $w->winkelcode)
                        <option selected value="{{$w->winkelcode}}">{{$w->winkelnaam}} {{$w->vestigingsplaats}}</option>
                    @else
                        <option value="{{$w->winkelcode}}">{{$w->winkelnaam}} {{$w->vestigingsplaats}}</option>
                    @endif
                @endforeach
            </select>
            <input type="submit" value="Zoek">
        </form>
    </div>

@stop
